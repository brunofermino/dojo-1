package br.com.itau.dojo1.serv.sentence;

import static junit.framework.TestCase.assertEquals;

import org.junit.Test;

public class SentenceTest {

	@Test
	public void testLastRepeatedWordWithoutWord() {
		final String word = new Sentence("aaaa bbbb cccc eeee ffff").lastRepeatedWord();
		assertEquals(word, "");
	}
}
