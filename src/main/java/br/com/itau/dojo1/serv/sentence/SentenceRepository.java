package br.com.itau.dojo1.serv.sentence;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface SentenceRepository extends CrudRepository<Sentence, Integer> {

}
