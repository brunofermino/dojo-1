package br.com.itau.dojo1.api;

public class SingleAttrGenericJson<T> {

	private final T value;

	public SingleAttrGenericJson(T value) {
		this.value = value;
	}

	public T getValue() {
		return value;
	}

}
