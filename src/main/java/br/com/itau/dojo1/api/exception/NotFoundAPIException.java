package br.com.itau.dojo1.api.exception;

import org.springframework.http.HttpStatus;

public class NotFoundAPIException extends APIException {

	private static final long serialVersionUID = 1L;

	public NotFoundAPIException() {
		super(HttpStatus.NOT_FOUND);
	}

}
