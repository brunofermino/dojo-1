package br.com.itau.dojo1.api.exception;

import org.springframework.http.HttpStatus;

public class BadRequestException extends APIException {

	private static final long serialVersionUID = 1L;

	private final String errorCode;

	private final Object[] args;

	public BadRequestException(String errorCode, Object... args) {
		super(HttpStatus.BAD_REQUEST);
		if (errorCode == null) {
			throw new IllegalArgumentException();
		}
		this.errorCode = errorCode;
		this.args = args;
	}

	public String getErrorCode() {
		return errorCode;
	}

	public Object[] getArgs() {
		return args;
	}
}
